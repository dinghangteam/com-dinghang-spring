package com.dinghang.spring.extension;

import com.alibaba.dubbo.common.extension.Adaptive;
import com.alibaba.dubbo.common.extension.ExtensionLoader;

/**
 * Created by wyf on 17/5/27.
 */
@Adaptive
public class AdaptiveExtension implements DinghangFirstExtension {

    public String sayHello(String name, String type) {
        ExtensionLoader extensionLoader = ExtensionLoader.getExtensionLoader(DinghangFirstExtension.class);
        DinghangFirstExtension extension= (DinghangFirstExtension) extensionLoader.getDefaultExtension();
        if(ExtensionType.DEFAULT.equals(type)){
            extension= (DinghangFirstExtension) extensionLoader.getExtension("default");
        }

        if(ExtensionType.OTHER.equals(type)){
            extension= (DinghangFirstExtension) extensionLoader.getExtension("other");
        }

        return extension.sayHello(name,type);
    }
}
