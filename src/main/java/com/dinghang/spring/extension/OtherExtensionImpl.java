package com.dinghang.spring.extension;

/**
 * Created by wyf on 17/5/27.
 */
public class OtherExtensionImpl implements DinghangFirstExtension {
    public String sayHello(String name, String type) {
        return type+" hello, other,"+name;
    }
}
