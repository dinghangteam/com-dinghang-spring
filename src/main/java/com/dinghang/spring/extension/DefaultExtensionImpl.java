package com.dinghang.spring.extension;

/**
 * Created by wyf on 17/5/27.
 */
public class DefaultExtensionImpl implements DinghangFirstExtension {
    public String sayHello(String name, String type) {
        return type+" hello, "+name;
    }
}
