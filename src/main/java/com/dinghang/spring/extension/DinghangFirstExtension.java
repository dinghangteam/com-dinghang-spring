package com.dinghang.spring.extension;

import com.alibaba.dubbo.common.extension.SPI;

/**
 * Created by wyf on 17/5/27.
 */
@SPI("other")
public interface DinghangFirstExtension {
     String sayHello(String name,String type);
}
