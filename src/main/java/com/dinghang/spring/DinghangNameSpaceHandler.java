package com.dinghang.spring;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * Created by wyf on 17/5/26.
 */
public class DinghangNameSpaceHandler  extends NamespaceHandlerSupport{

    public void init() {
        registerBeanDefinitionParser("people",new PeopleBeanDefinitionParser());
    }
}
